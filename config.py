import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'This is an INSECURE secret!! DO NOT use this in production!!'
