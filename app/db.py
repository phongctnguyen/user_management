import pymongo

# Set up database
client = pymongo.MongoClient("localhost", 27017)
db = client.user_database

# start mongo
# sudo brew services start mongodb-community

# check collection


def check_collection_exist(collection):
    if collection in db.list_collection_names():
        return True
    else:
        return False

# find all documents


def find_all(collection, dict=None):
    if check_collection_exist(collection):
        return db[collection].find(dict)
    else:
        return None

# find one document


def find_one(collection, dict=None):
    if check_collection_exist(collection):
        # print('a')
        return db[collection].find_one(dict)
    else:
        # print('b')
        return None

# save document into database


def save_database(collection, dict):
    return db[collection].insert(dict)

# update document


def update_database(collection, query_dict, dict):
    return db[collection].update(query_dict, {"$set": dict})

# delete document


def delete_database(collection, dict):
    return db[collection].delete_one(dict)
