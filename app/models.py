from app import db
from app import login
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
import onetimepass


class User():
    def __init__(
            self,
            username,
            email,
            password,
            secret_key=None,
            status="active"):
        self.username = username
        self.email = email
        self.password = generate_password_hash(password)
        self.secret_key = secret_key
        self.status = status

    # def check_password(self, password):
    #     return check_password_hash(self.password, password)

    # def get_totp_uri(self):
    #     return 'otpauth://totp/2FA-Demo:{0}?secret={1}&issuer=2FA-Demo' \
    #         .format(self.username, self.secret_key)

    # def verify_totp(self, token):
    #     return onetimepass.valid_totp(token, self.secret_key)
