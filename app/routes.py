from flask import Flask, render_template, send_from_directory, request, redirect, session, flash, url_for, jsonify
from flask_login import current_user, login_user, logout_user, login_required
from app import app, db, api, redis_db
from app.forms import LoginForm, RegistrationForm, EditProfileForm, OTPForm
from app.models import User
from werkzeug.urls import url_parse
from werkzeug.security import generate_password_hash, check_password_hash
from flask_bootstrap import Bootstrap
from io import BytesIO
import pyotp
import pyqrcode
from bson.json_util import dumps
from datetime import timedelta

# Init setting
# session.permanent = True
# app.permanent_session_lifetime = timedelta(minutes=1)

# Index page
@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home')

    # Lấy hostname từ request
    # host_name = request.headers.get("Host")
    # return host_name.split(":")[0]

# Login account
@app.route('/login', methods=['GET', 'POST'])
def login():
    if 'logged_in' in session and session['logged_in']:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        host_name = request.headers.get("Host")
        username = form.username.data
        password = form.password.data
        user = db.find_one("users", {"username": username})
        # session_id = user["_id"]
        # print(user["_id"])

        if (user is not None):
            is_correct_password = check_password_hash(
                str(user['password']), str(password))
            # print(is_correct_password)
        # print(generate_password_hash(password))
        # print(form.password.data)

        if user is None or not is_correct_password:
            flash('Invalid username or password')
            return redirect(url_for('login'))
        else:
            print("Username: %s - Login Time: %s" %
                  (username, redis_db.redis_db.get(username)))
            
            '''
            Use redis setex
            '''
            # if redis_db.redis_db.get(username) and int(
            #         redis_db.redis_db.get(username)) >= 3:
            #     flash('Login too much!')
            #     # redis_db.redis_db.set(username, None)
            #     return redirect(url_for('login'))
            # else:
            #     session['logged_in'] = False
            #     session['username'] = username
            #     print(redis_db.redis_db.get(username))
            #     # After 3 minutes, redis_db will set value of username = None
            #     if redis_db.redis_db.get(username) is None:
            #         redis_db.redis_db.setex(
            #             username, timedelta(minutes=3), value=1)
            #     else:
            #         redis_db.redis_db.incr(username)
            #     return redirect(url_for('checkotp'))

            '''
            Use redis expire
            '''
            check_host = host_name.split(":")[0] == 'phongnct'
            if redis_db.redis_db.get(username) and int(
                    redis_db.redis_db.get(username)) >= 3 and check_host:
                flash('Login too much!')
                return redirect(url_for('login'))
            else:
                session['logged_in'] = False
                session['username'] = username
                print(redis_db.redis_db.get(username))
                print('a')
                # host_name.split(":")[0]
                # print(redis_db.redis_db.get(username))
                # After 3 minutes, redis_db will set value of username = None
                if redis_db.redis_db.get(username) is None and check_host:
                    redis_db.redis_db.set(username, 0)
                    redis_db.redis_db.expire(username, 120)
                    # print(redis_db.redis_db.get(username))
                elif redis_db.redis_db.get(username) is not None and check_host:
                    redis_db.redis_db.incr(username)
                return redirect(url_for('checkotp'))
            
    return render_template('login.html', title='Sign In', form=form)

# Check OTP after login with username & password
@app.route('/checkotp', methods=['GET', 'POST'])
def checkotp():
    user = db.find_one("users", {"username": session['username']})
    print(user)
    # secret = pyotp.random_base32()
    totp = pyotp.TOTP(user['secret_key'])
    otp = totp.now()
    print(otp)
    form = OTPForm()
    if form.validate_on_submit():
        otp_submit = form.otp_submit.data
        if otp_submit and totp.verify(otp_submit):
            session['logged_in'] = True
            api.telegram_bot_sendtext(
                user['username'] +
                ' Logged In')  # send noti to telegram chatbot
            return redirect(url_for('index'))
        else:
            return redirect(url_for('checkotp'))
    return render_template('otp.html', title='Check OTP', form=form)

# Logout
@app.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

# Register
@app.route('/register', methods=['GET', 'POST'])
def register():
    if 'logged_in' in session and session['logged_in']:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        password = form.password.data
        user = User(username=username, email=email,
                    password=password, secret_key=pyotp.random_base32())
        session['username'] = user.username
        db.save_database("users", user.__dict__)
        # redis_db.redis_db.set(user.username, 0)
        # flash('Congratulations, you are now a registered user!')
        return redirect(url_for('twofactor'))
    return render_template('register.html', title='Register', form=form)

# Check QR Code after register
@app.route('/twofactor')
def twofactor():
    if 'logged_in' in session and session['logged_in']:
        return redirect(url_for('index'))
    user = db.find_one("users", {"username": session['username']})
    print(user)
    if user is None:
        return redirect(url_for('index'))
    # since this page contains the sensitive qrcode, make sure the browser
    # does not cache it
    return render_template('two_factor_setup.html'), 200, {
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': '0'}

# Gen QR Code Image
@app.route('/qrcode')
def qrcode():
    user = db.find_one("users", {"username": session['username']})
    qr_code = pyotp.totp.TOTP(
        user['secret_key']).provisioning_uri(
        user['email'],
        issuer_name="User Management App")
    print(qr_code)
    url = pyqrcode.create(qr_code)
    stream = BytesIO()
    url.svg(stream, scale=5)
    return stream.getvalue(), 200, {
        'Content-Type': 'image/svg+xml',
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        'Pragma': 'no-cache',
        'Expires': '0'}

# Show User Profile
@app.route('/user')
def user():
    # if 'logged_in' in session and session['logged_in'] == True:
    # print(session['logged_in'])
    user = db.find_one("users", {"username": session['username']})
    # print(user)
    return render_template('profile.html', user=user)

# User can edit their profile and database will update
@app.route('/edit_profile', methods=['GET', 'POST'])
def edit_profile():
    form = EditProfileForm()

    if form.validate_on_submit():
        username = form.username.data
        email = form.email.data
        password = form.password.data
        current_name = session['username']
        user = User(username=username, email=email, password=password).__dict__
        user_exist = db.find_one("users", {"username": session['username']})
        # print(type(user_exist))
        dict_user = {}

        if user['username'] and user['username'] != user_exist['username']:
            dict_user['username'] = user['username']
            session['username'] = user['username']
            # print(session['username'])
        # if user['password'] and user['password'] != user_exist.password:
        #     dict_user.username =  user['password']
        if user['email'] and user['email'] != user_exist['email']:
            dict_user['email'] = user['email']

        # print(dict_user)

        db.update_database("users", {"username": current_name}, dict_user)
        flash('Your changes have been saved.')
        return redirect(url_for('edit_profile'))

    elif request.method == 'GET':
        user = db.find_one("users", {"username": session['username']})
        form.username.data = user['username']
        form.email.data = user['email']

    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)

# Admin delete user
@app.route('/account', methods=['GET', 'POST', 'DELETE'])
def account():
    if request.method == "DELETE":
        name = request.form.get("name")
        print(name)
        db.delete_database("users", {"username": name})
    users = db.find_all("users")
    return render_template('user.html', users=users)
