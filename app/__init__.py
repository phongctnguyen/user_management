from flask import Flask
from config import Config
from flask_login import LoginManager


app = Flask(__name__)
DEBUG = True
app.config.from_object(Config)
login = LoginManager(app)
login.login_view = 'login'

from app import routes

if __name__ == "__main__":
    app.run(debug=True)

